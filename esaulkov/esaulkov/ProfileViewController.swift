//
//  SecondViewController.swift
//  esaulkov
//
//  Created by Андрей on 25.09.2018.
//  Copyright © 2018 Tinkoff. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController {

    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var textprofileLabel: UILabel!
    @IBOutlet weak var profileLabel: UILabel!
    //@IBOutlet weak var placeholderImage: UIImageView!
@IBOutlet weak var editButton: UIButton!

    @IBAction func editButtonPush(_ sender: Any) {
      print("TODO: editButton In near future")
        UserTune()
        
    }
    
    func UserTune (){
        profileLabel.text = "Kermit"
        profileLabel.font = UIFont(name: "System", size: 24)
        profileLabel.font = UIFont.boldSystemFont(ofSize: 24)
        
        textprofileLabel.text = "some text rereegegerwefewkfkwefnewf newfergregeergergerg ekvervjer vj erjv ejrv jer vj erjv erjv jer vj erjv erjv erjv ejrv j erj erjv ejrv er vierivierviervirenviernvinerivnerinvi erivneirviernvierivnerinviernviervienrvinerivnerinv"
            
        textprofileLabel.numberOfLines = 0
        textprofileLabel.sizeToFit()
        
        userImage.image = #imageLiteral(resourceName: "kermit")
        userImage?.contentMode = .scaleAspectFill
    }
    
    func SetupUI(){
        profileLabel.textColor = UIColor.black
        profileLabel.font = UIFont(name: "System", size: 24)
        
        textprofileLabel.textColor = UIColor.gray
        textprofileLabel.font = UIFont(name: "System", size: 20)
        
        
        editButton.setTitle("Редактировать", for: .normal)
        editButton.setTitleColor(UIColor.darkGray, for: .normal)
        editButton.titleLabel?.font = UIFont.init(name: "System", size:18)
        editButton.layer.cornerRadius = 8.0
    }
    
    @IBOutlet weak var photoButton: UIButton!
    
    @IBAction func photoButtonPush(_ sender: Any) {
        print("TODO: photoButton In near future")
    }
    //other IBOutlets
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
       //пропишем функцию входа
        SetupUI()
        print(editButton.frame)
        
        // Do any additional setup after loading the view, typically from a nib.
        
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        print(editButton.frame)
        /* координаты фрейм меняются, потому что изменяется Label c текстом, относительно
 которого распожена кнопка */
        
    }
 
   
    
}
